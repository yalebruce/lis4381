> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development 

## Yale Bruce

### LIS4381 Requirements:

*Course Work Links*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    * Install AMPPS
        * Screenshot of AMPPS running 
    * Install Java Developer Kit
        * Screenshot of JDK java "Hello"
    * Install Visual Studio Code
    * Install Android Studio
        * Screenshot of Android Studio "My First App" 
    * provide git command descriptions
    * Create Bitbucket Repository
    * Complete Bitbucket tutorial (bitbucketstationlocations)

2. [A2 README.md](a2/README.md "My A2 README.md file")
    * Screenshot of Brushetta recipe screen 1 
    * Screenshot of Brushetta recipe screen 2
    * Screenshot of skillset 1 even or odd
    * Screenshot of skillset 2 layers 
    * Screenshot of skillset 3 arrays and loops

3. [A3 README.md](a3/README.md "My A3 README.md file")
    * Screenshot of MySQL workbench
		* ERD Diagram 
	* Screenshot of android studio app
		* first user interface 
		* second user interface 
	* Screenshot of 10 records for each table 
		* petstore table 
		* customer table 
		* pet table 
	* Links to the following files: 
		* a3.mwb
		* a3.sql

4. [A4 README.md](a4/README.md "My A4 README.md file")
    * Create a mobile web app with bootstrap validation 
		* Screenshot of mainpage 
		* Screenshot of failed validation 
		* Screenshot of passed validation 
	* Screenshot of skillsets 10, 11, & 12

5. [A5 README.md](a5/README.md "My A5 README.md file")
    * Due: November 18, 2021

6. [P1 README.md](p1/README.md "My P1 README.md file")
    * Create a business card with a photo, contact information and special interests
		* Screenshot of first interface 
		* Screenshot of second interface
	* Screenshot of skillsets 7, 8, & 9

7. [P2 README.md](p2/README.md "My P2 README.md file")
    * Due: December 02, 2021



