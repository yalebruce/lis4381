> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web Applications Development 

## Yale Bruce

### Assignment 2 Requirements:

*Sub-Heading:*

1. Develop application on android that 
	a)displays Bruschetta Recipe title on first page
	b)displaus the image provided on canvas 
	c)displays "view" button that links to second page
	d)displays Bruschetta Recipe title on second page 
	e)lists items in recipe 
	f)displays a description 
2. Screenshot of skillset 1
3. Screenshot of skillset 2
4. Screenshot of skillset 3

#### README.md file should include the following items:

* Screenshot of Bruschetta Recipe screen 1
* Screenshot of Bruschetta Recipe screen 1
* Screenshot of skillset 1
* Screenshot of skillset 2
* Screenshot of skillset 3

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:

*Screenshot of Bruschetta recipe 1*:

![Bruschetta recipe Screenshot](img/healthy_recipes1.PNG)

*Screenshot of Bruschetta recipe 2*:

![Bruschetta recipe Screenshot](img/healthy_recipes2.PNG)

*Screenshot of Skillset 1*:

![Skillset 1 Screenshot](img/skillset1.PNG)

*Screenshot of Skillset 2*:

![Skillset 2 Screenshot](img/skillset2.PNG) 

*Screenshot of Skillset 3*:

![Skillset 3 Screenshot](img/skillset3.PNG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
