import java.util.Scanner; 

public class Methods 
{
    public static void getRequirements()
    {

    System.out.println("Developer: Yale Bruce");
    System.out.println("Program searches user-entered integer w/in array of integers.");
    System.out.println("Create an array with the following values: 3, 2, 4, 99, -1, -5, 3, 7");

    System.out.println();
    }

    public static void searchArray()
    { 
        int nums[] = {3, 2, 4, 99, -1, -5, 3, 7};
        Scanner sc = new Scanner(System.in);
        int search;

        System.out.print("Array length: " + nums.length);

        System.out.print("\nEnter search values:: ");
        search = sc.nextInt(); 

        System.out.println();
        for (int i = 0 ; i < nums.length; i++)
        { 
            if(nums[i] == search) 
            { 
                System.out.println(search + " found at index " + i);
            } 
            else
            { 
                System.out.println(search + " *not* found at index " + i);
            }

        }
    }
}