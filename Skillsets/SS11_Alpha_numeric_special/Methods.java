import java.util.ArrayList;
import java.util.Scanner; 

public class Methods 
{
    public static void getRequirements()
    {

    System.out.println("Developer: Yale Bruce");
    System.out.println("Program populates array list of strings with user-entered animal type values.");
    System.out.println("Examples: Polar bear, guinea pig, dog, car, bird.");
    System.out.println("Program continues to collect user-entered values until user enters \'n\'.");
    System.out.println("Program displays array list values after each iteration, including size.");

    System.out.println();
    }

    public static void createArrayList()
    { 
        Scanner sc = new Scanner(System.in);
        ArrayList<String> obj = new ArrayList<String>();
        String myStr = "";
        String choice = "y";
        int num = 0; 

        while (choice.equals("y")) 
        {
            System.out.print("Enter animal type: ");
            myStr = sc.nextLine();
            obj.add(myStr); 
            System.out.println("ArrayList elements:" + obj + "\nArrayList Size = " + num);
            System.out.print("\nContinue? Enter y or n: ");
            choice = sc.next().toLowerCase();
            sc.nextLine();
        }

        

        
    }
}