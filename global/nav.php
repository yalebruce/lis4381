	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">			
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#" target="_self">Home</a>
			</div>

			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="../index.php">My portfolio</a></li>
					<li><a href="../a1/index.php">a1</a></li>
					<li><a href="../a2/index.php">a2</a></li>
					<li><a href="../a3/index.php">a3</a></li>
					<li><a href="../a4/index.php">a4</a></li>
					<li><a href="../a5/index.php">a5</a></li>
					<li><a href="../p1/index.php">p1</a></li>
					<li><a href="../p2/index.php">p2</a></li>
					<li><a href="../test/index.php">Test</a></li>					
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>

<?php
date_default_timezone_set('America/New_York');
$today = date("m/d/y g:ia");
echo $today;
 ?>
	
