> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Yale Bruce

### Assignment 4 Requirements:

*Sub-Heading:*

1. Clone assignment starter files
2. Open index.php and review code
3. Create a favicon 

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1
* Screenshot of mainpage, failed validation, passed validation 
* link to local web app 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>
#### Assignment Screenshots:

*Screenshot of mainpage http://localhost*:

![Mainpage Screenshot](img/mainpage.PNG)

*Screenshot of failed validation*:

![Failed validation Screenshot](img/)

*Screenshot of passed validation*:

![Passed Validation Installation Screenshot](img/Passedval.PNG)

*Screenshot of skillset 10*:

![Skillset 10 Screenshot](img/skillset10.PNG)

*Screenshot of skillset 11*:

![Skillset 11 Screenshot](img/skillset11.PNG)

*Screenshot of skillset 12*:

![Skillset 12 Screenshot](img/skillset12.PNG)



#### Tutorial Links:


