> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Web Applications Development

## ybb19 / Yale Bruce

### Assignment 1 Requirements:

*Four parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installation
3. Questions
4. Bitbucket Repository Links:
    
    a) https://bitbucket.org/yalebruce/lis4381/src/master/
    
    b) https://bitbucket.org/yalebruce/bitbucketstationlocations/src/master/

#### README.md file should include the following items:

* Screenshot of AMPPS application running

* Screenshot of running JDK "java Hello"

* Screenshot of running Android Studio "My First App"

* git commands w/short descriptions
    1. git init: Create an empty Git repository or reinitialize an existing one
    2. git status: Show the working tree status
    3. git add: Add file contents to the index
    4. git commit: Record changes to the repository
    5. git push: Update remote refs along with associated objects
    6. git pull: Fetch from and integrate with another repository or a local branch
    7. git help: Display help information about Git


#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/php_ss.PNG)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/Hello_java_ss.PNG)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/My_first_app.PNG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
