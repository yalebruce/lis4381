> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Yale Bruce

### Project # Requirements:

*Sub-Heading:*

* Create a business card on android studio with two interfaces
	* First interface shows name, a headshot image, and a button that takes you to the second interface
	* Second interface shows student status, contact information, and special interests
* Screenshots of skillset 7-9 

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements
* Screenshot of running application’s first user interface
* Screenshot of running application’s second user interface
* Screenshot of skillsets

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of first user interface*:

![First interface Screenshot](img/interface1.PNG)

*Screenshot of second user interface*:

![Second interface Screenshot](img/interface2.PNG)

*Screenshot of skillsets 7-9*:

![Skillset 7 Screenshot](img/skillset_7.PNG)

![Skillset 8 Screenshot](img/skillset_8.PNG)

![Skillset 9 Screenshot](img/skillset_9.PNG)



#### File Links:


