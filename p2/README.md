> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Yale Bruce

### Project 2 Requirements:

*Sub-Heading:*

* Open index.php and review code

#### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1; 
2. Screenshots as per below examples; 
3. Link to local lis4381 web app:

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Homepage*:

![Homepage Screenshot](img/mainpage.PNG)

*Screenshot of index.php*:

![index.php Screenshot](img/index.png)

*Screenshot edit_petstore*:

![edit_petstore Screenshot](img/editpetstore.png)

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/passedvalidation.png)

*Screenshot of Deleted record*:

![Deleted record Screenshot](img/passedvalidation.png) 

*Screenshot of Successfully deleted record*:

![Successfully deleted Screenshot](img/successfullydeleted.png)



#### File Links:


