<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Yale Bruce">
    <link rel="icon" href="favicon.ico">

		<title>YBB19B - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> The expected norm...(*be sure* to copy the assignment requirements here!) 
				</p>

				<h4>First interface</h4>
				<img src="img/healthy_recipes1.png" class="img-responsive center-block" alt="Bruschetta Recipe">

				<h4>Second interface</h4>
				<img src="img/healthy_recipes2.png" class="img-responsive center-block" alt="Bruschetta Recipe menu">

				<h4>Skillset 1</h4>
				<img src="img/skillset1.png" class="img-responsive center-block" alt="Skillset 1">

				<h4>Skillset 2</h4>
				<img src="img/skillset2.png" class="img-responsive center-block" alt="Skillset 2">

				<h4>Skillset 3</h4>
				<img src="img/skillset3.png" class="img-responsive center-block" alt="Skillset 3">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
