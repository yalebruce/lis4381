> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Yale Bruce

### Assignment # Requirements:

*Sub-Heading:*

1. Create a relational database with the tables: "Pestore" "Customer" and "Pet" 
	* store at least 10 records in each table 
2. Create an android application with two interfaces that calculates the total cost of ticket value entered
3. Screenshots of skillset 4-6 

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements
* Screenshot of ERD
* Screenshot of running application’s first user interface
* Screenshot of running application’s second user interface
* Screenshots of 10 records for each table—use select from each table 
* Links to the following files:
	* a3.mwbb 
	* a3.sql

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD Screenshot](img/erd_diagram_ss.PNG)

*Screenshot of first user interface*:

![First interface Screenshot](img/ticket_interface1.PNG)

*Screenshot of second user interface*:

![Second interface Screenshot](img/ticket_interface2.PNG)

*Screenshot of 10 records for each table*:

![Petstore records Screenshot](img/petstore_ss.PNG)

![Customer records Screenshot](img/customer_ss.PNG)

![Pet records Screenshot](img/pet_ss.PNG)

*Screenshot of skillsets 4-6*:

![Skillset 4 Screenshot](img/skillset_4.PNG)

![Skillset 5 Screenshot](img/skillset_5.PNG)

![Skillset 6 Screenshot](img/skillset_6.PNG)



#### File Links:

*Link to a3 mwb*
[Petstore databse](../Skillsets/a3_erd "a3.mwb and a3.sql")

